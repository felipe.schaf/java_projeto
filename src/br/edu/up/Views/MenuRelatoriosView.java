
package br.edu.up.Views;

import br.edu.up.Controllers.DepartamentoController;
import br.edu.up.Controllers.FuncaoController;
import br.edu.up.Controllers.FuncionarioController;
import br.edu.up.Controllers.RegistroController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class MenuRelatoriosView {
    public void Menu() throws Exception {
        Scanner scanner = new Scanner(System.in);
        RegistroController registroController = new RegistroController();

        int opcao;
        do {
            System.out.print("\033[H\033[2J");
            System.out.flush();
            System.out.println("Selecione uma opção:");
            System.out.println("1. Relatório de Batidas Geral");
            System.out.println("2. Relatório de Batidas por Período");
            System.out.println("3. Relatório de Funcionários");
            System.out.println("4. Relatório de Departamentos");
            System.out.println("5. Relatório de Funções");
            System.out.println("");
            System.out.println("0. Voltar ao menu principal");
            System.out.println("");
            System.out.print("\u001B[33mOpção:\u001B[0m");
            opcao = scanner.nextInt();

            switch (opcao) {
                case 1:
                    registroController.ExibirListaRegistros();
                    break;
                case 2:
                    System.out.print("\033[H\033[2J");  
                    System.out.flush();  
                    Boolean dadosConferem = false;
                    LocalDateTime dateInicializa = LocalDateTime.of(1900, 01, 01, 01, 01);
                    LocalDateTime dateTimeInicio = dateInicializa;
                    LocalDateTime dateTimeFim = dateInicializa;

                    while(dadosConferem == false)
                    {
                        dadosConferem = true;
                        System.out.println("");
                        System.out.print("Padrão de Data: " + "\u001B[32mdd/MM/yyyy HH:mm\u001B[0m ");
                        scanner.nextLine();
                        System.out.println("");
                        System.out.println("Informe o fim do período que deseja seguindo o padrão de data:");
                        System.out.print("\u001B[33mOpção:\u001B[0m ");
                        String inicio = scanner.nextLine();
                

                        System.out.println("");
                        System.out.println("Informe o fim do período que deseja seguindo o padrão de data:");
                        System.out.print("\u001B[33mOpção:\u001B[0m ");
                        String fim = scanner.nextLine();

                        try
                        {

                            DateTimeFormatter formatterIncio = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
                            dateTimeInicio = LocalDateTime.parse(inicio, formatterIncio);

                            DateTimeFormatter formatterFim = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
                             dateTimeFim = LocalDateTime.parse(fim, formatterFim);
                        }
                        catch(Exception ex)
                        {
                            dadosConferem = false;
                            System.out.println("Alguma data foi informada fora do padrão. O correto é seguir este padrão: dd/MM/yyyy HH:mm");
                            System.out.println("");
                            System.out.println("\u001B[mAperte qualquer tecla para tentar novamente...\u001B[0m");
                            scanner.nextLine();
                            scanner.nextLine();
                            System.out.print("\033[H\033[2J");  
                            System.out.flush();  
                        }
                    }

                    try
                    {
                        
                        if(dateTimeInicio.isAfter(dateInicializa) && dateTimeFim.isAfter(dateInicializa))
                        {
                            registroController.ExibirListaRegistrosPorPeriodo(dateTimeInicio, dateTimeFim);
                        }else
                        {
                            throw new Exception("Alguma data está errada");
                        }

                    }
                    catch(Exception ex)
                    {
                        throw new Exception(ex.getMessage());
                    }
                    break;
                case 3:
                    FuncionarioController funcionarioController = new FuncionarioController();
                    System.out.println("");
                    funcionarioController.ExibirListaFuncionarios();
                    break;
                case 4:
                    DepartamentoController departamentoController = new DepartamentoController();
                    System.out.println("");
                    departamentoController.ExibirListaDepartamentos();
                    break;
                case 5:
                    FuncaoController funcaoController = new FuncaoController();
                    System.out.println("");
                    funcaoController.ExibirListaFuncoes();
                    break;
                case 0:
                    MenuPrincipalView menuPrincipalView = new MenuPrincipalView();
                    menuPrincipalView.Menu();
                    break;
                default:
                    System.out.println("Opção inválida!");
                    break;
            }
            System.out.println("");
            System.out.println("");
            System.out.println("Aperte qualquer tecla pra voltar visualizar as opções novamente...");
            scanner.nextLine();
            scanner.nextLine();
        } while (opcao != 0);
    }
}
