package br.edu.up.Views;

import br.edu.up.Controllers.DepartamentoController;
import br.edu.up.Models.Departamento;
import java.util.Optional;
import java.util.Scanner;

public class CadastrarDepartamentoView {

public void CadastrarNovoDepartamento() throws Exception {
        Scanner scanner = new Scanner(System.in);
        DepartamentoController departamentoController = new DepartamentoController();


        var departamentos = departamentoController.BuscarListaDepartamentos();
        Optional<Departamento> departamentOptional = null; 
        String Descricao = "";


        try {

            Boolean dadosConferem = false;
            while(dadosConferem == false)
            {
                dadosConferem = true;

                System.out.println("");
                System.out.println("Digite a Descrição: ");
                System.out.print("\u001B[33mOpção:\u001B[0m"); Descricao = scanner.nextLine();

                final String descricaoFinal = Descricao;
                Departamento departamento = null;
                    try
                    {   
                        departamentOptional =  departamentos.stream()
                                            .filter(d -> d.getDescricao().equals(descricaoFinal))
                                            .findFirst();
                        if(departamentOptional != null)
                             departamento = departamentOptional.get();
                    }
                    catch(Exception e)
                    {
                            
                    }

                System.out.println("");
                System.out.println("SALVAR = 1  |  CANCELAR = 0");
                System.out.print("\u001B[33mOpção:\u001B[0m"); var opcao = scanner.nextInt();

                if(opcao == 1)
                {

                    if(departamento != null)
                    {
                        System.out.println("");
                        System.out.println("\u001B[31mFalha ao finalizar cadastro.\u001B[0m Departamento já cadastrado.");
                        dadosConferem = false;
                    }

                    if(dadosConferem == false)
                    {
                        System.out.println("");
                        System.out.println("\u001B[mAperte qualquer tecla para tentar novamente...\u001B[0m");
                        scanner.nextLine();
                        scanner.nextLine();
                        System.out.print("\033[H\033[2J");  
                        System.out.flush();  
                    }
                    
                }
                else
                {
                    dadosConferem = false;
                }
            }
            

 
                Departamento departamentoCadastrar = new Departamento();
                departamentoCadastrar.setDescricao(Descricao);
                departamentoController.GravarNovoDepartamento(departamentoCadastrar);

        } catch (Exception e) {
            System.out.println("Erro: " + e.getMessage());
        } finally {
            System.out.println("");
            System.out.println("Aperte qualquer tecla para voltar ao menu principal...");
            scanner.nextLine();
            scanner.nextLine();
            System.out.print("\033[H\033[2J");  
            System.out.flush();  
            MenuPrincipalView menu = new MenuPrincipalView();
            menu.Menu();
        }
    }

}
