package br.edu.up.Views;

import java.time.LocalDateTime;
import java.util.Scanner;

import br.edu.up.Controllers.FuncionarioController;
import br.edu.up.Controllers.RegistroController;
import br.edu.up.Models.Funcao;
import br.edu.up.Models.Funcionario;
import br.edu.up.Models.Registro;

public class RegistrarPontoView {

    @SuppressWarnings("null")
    public void RegistrarPonto() throws Exception {
        Scanner scanner = new Scanner(System.in);
        FuncionarioController funcionarioController = new FuncionarioController();
        RegistroController registroController = new RegistroController();

        System.out.print("\033[H\033[2J");
        System.out.flush();

        try {
            Funcionario funcionario = null;
            int Matricula = 0;
            String Motivo = "";
            LocalDateTime Data = LocalDateTime.now();

            Boolean dadosConferem = false;
            while (dadosConferem == false) {
                dadosConferem = true;

                System.out.println("");
                System.out.println("Digite a Matrícula do Funcionário: ");
                System.out.print("\u001B[33mOpção:\u001B[0m");
                Matricula = scanner.nextInt();

                try {
                    funcionario = funcionarioController.ObterPorMatricula(Matricula);
                    if (funcionario != null) {
                        System.out.println("Nome: " + funcionario.getNome());
                        System.out.println("");
                        System.out.println("Confirma funcionário? SIM = 1  |  NÃO = 0:");
                        var opcao = scanner.nextInt();

                        if (opcao == 1) {

                            System.out.println("Informe o motivo: ");
                            System.out.print("\u001B[33mOpção:\u001B[0m");
                            Motivo = scanner.nextLine();
                            Motivo = scanner.nextLine();

                        } else {
                            dadosConferem = false;
                        }
                    } else {
                        throw new Exception("Funcionário não encontrado");
                    }
                } catch (Exception e) {
                    System.out.println("Erro: " + e.getMessage());

                }

            }

            Registro registro = new Registro();
            registro.setFuncionario(funcionario);
            registro.setData(Data);
            registro.setMotivo(Motivo);
            registroController.GravarNovoRegistro(registro);

        } catch (Exception e) {
            System.out.println("Erro: " + e.getMessage());
            System.out.println("Aperte qualquer tecla para tentar novamente...");
            scanner.nextLine();
            scanner.nextLine();
            RegistrarPontoView tentarNovamente = new RegistrarPontoView();
            tentarNovamente.RegistrarPonto();
        } finally {
            System.out.println("");
            System.out.println("Aperte qualquer tecla para voltar ao menu principal...");
            scanner.nextLine();
            scanner.nextLine();
            System.out.print("\033[H\033[2J");
            System.out.flush();
            MenuPrincipalView menu = new MenuPrincipalView();
            menu.Menu();
        }

    }

}
