
package br.edu.up.Views;

import java.util.Scanner;

public class MenuPrincipalView {
    public void Menu() throws Exception
    {
        Scanner scanner = new Scanner(System.in);
        // RelatorioController relatorioController = new RelatorioController();

        int opcao;
        do {
            System.out.print("\033[H\033[2J");  
            System.out.flush();  
            System.out.println("Selecione uma opção:");
            System.out.println("1. Registrar Ponto");
            System.out.println("2. Cadastrar Funcionário");
            System.out.println("3. Cadastrar Função");
            System.out.println("4. Cadastrar Departamento");
            System.out.println("5. Relatórios");
            System.out.println("0. Sair");

            System.out.print("\u001B[33mOpção:\u001B[0m"); opcao = scanner.nextInt();

            switch (opcao) {
                case 1:
                     RegistrarPontoView  registrarPontoView = new RegistrarPontoView();
                     registrarPontoView.RegistrarPonto();
                    break;
                case 2:
                        CadastrarFuncionarioView  cadastrarFuncionarioView = new CadastrarFuncionarioView();
                        cadastrarFuncionarioView.CadastrarNovoFuncionario();
                    break;
                case 3:
                        CadastrarFuncaoView  cadastrarFuncaoView = new CadastrarFuncaoView();
                        cadastrarFuncaoView.CadastrarNovaFuncao();
                    break;
                case 4:
                        CadastrarDepartamentoView cadastrarDepartamentoView = new CadastrarDepartamentoView();
                        cadastrarDepartamentoView.CadastrarNovoDepartamento();
                    break;
                case 5:
                        MenuRelatoriosView menuRelatoriosView = new MenuRelatoriosView();
                        menuRelatoriosView.Menu();
                    break;
                case 0:
                    System.out.println("Saindo...");
                    break;
                default:
                    System.out.println("Opção inválida!");
                    break;
            }
        } while (opcao != 0);
    }
}
