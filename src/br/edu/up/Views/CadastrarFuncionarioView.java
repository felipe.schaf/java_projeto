package br.edu.up.Views;

import br.edu.up.Controllers.DepartamentoController;
import br.edu.up.Controllers.FuncaoController;
import br.edu.up.Controllers.FuncionarioController;
import br.edu.up.Models.Departamento;
import br.edu.up.Models.Funcao;
import br.edu.up.Models.Funcionario;
import java.util.Optional;
import java.util.Scanner;

public class CadastrarFuncionarioView {


    public void CadastrarNovoFuncionario() throws Exception {
        Scanner scanner = new Scanner(System.in);
        DepartamentoController departamentoController = new DepartamentoController();
        FuncaoController funcaoController = new FuncaoController();
        FuncionarioController funcionarioController = new FuncionarioController();
        
        var departamentos = departamentoController.BuscarListaDepartamentos();
        var funcoes = funcaoController.BuscarListaFuncao();
        Optional<Departamento> departamento = null; 
        Optional<Funcao> funcao = null; 
        String Nome = "";
        int Matricula = 0;

        try {

            Boolean dadosConferem = false;
            while(dadosConferem == false)
            {
                dadosConferem = true;

                System.out.println("");
                System.out.println("Digite a matrícula: ");
                System.out.print("\u001B[33mOpção:\u001B[0m"); Matricula = scanner.nextInt();
                scanner.nextLine();

                System.out.println("");
                System.out.println("Digite o nome: ");
                System.out.print("\u001B[33mOpção:\u001B[0m"); Nome = scanner.nextLine();
                
                Funcionario funcionario = null;
                try
                {
                    funcionario = funcionarioController.ObterPorMatricula(Matricula);
                }
                catch(Exception e)
                {
                        
                }
                System.out.println("");
                System.out.println("Selecione o departamento: ");
                    departamentoController.ExibirListaDepartamentos();
                    System.out.print("\u001B[33mOpção:\u001B[0m"); int departamentoId = scanner.nextInt(); 
                        try
                        {
                            departamento =  departamentos.stream()
                            .filter(d -> d.getId() == departamentoId)
                            .findFirst();
                        }
                        catch(Exception e)
                        {
                                
                        }

                System.out.println("");
                System.out.print("Selecione a função: ");
                    funcaoController.ExibirListaFuncoes();
                    System.out.print("\u001B[33mOpção:\u001B[0m"); int funcaoId = scanner.nextInt(); 
                        try
                        {   
                            funcao =  funcoes.stream()
                            .filter(d -> d.getId() == funcaoId)
                            .findFirst();
                        }
                        catch(Exception e)
                        {
                                
                        }

                System.out.println("");
                System.out.println("SALVAR = 1  |  CANCELAR = 0");
                System.out.print("\u001B[33mOpção:\u001B[0m"); var opcao = scanner.nextInt();

                if(opcao == 1)
                {
                    if(funcionario != null)
                    {
                        System.out.println("");
                        System.out.println("\u001B[31mFalha ao finalizar cadastro.\u001B[0m Matrícula já cadastrada em outro funcionário");
                        dadosConferem = false;
                    }

                    if(departamento == null)
                    {
                        System.out.println("");
                        System.out.println("\u001B[31mFalha ao finalizar cadastro.\u001B[0m Departamento Inválido");
                        dadosConferem = false;
                    }

                    if(funcao == null)
                    {
                        System.out.println("");
                        System.out.println("\u001B[31mFalha ao finalizar cadastro.\u001B[0m Função Inválido");
                        dadosConferem = false;
                    }

                    if(dadosConferem == false)
                    {
                        System.out.println("");
                        System.out.println("\u001B[mAperte qualquer tecla para tentar novamente...\u001B[0m");
                        scanner.nextLine();
                        scanner.nextLine();
                        System.out.print("\033[H\033[2J");  
                        System.out.flush();  
                    }
                    
                }
                else
                {
                    dadosConferem = false;
                }
            }
            

 
                Funcionario funcionario = new Funcionario(Matricula, Nome, departamento.get(), funcao.get());
                funcionarioController.GravarNovoFuncionario(funcionario);
        } catch (Exception e) {
            System.out.println("Erro: " + e.getMessage());
        } finally {
            System.out.println("");
            System.out.println("Aperte qualquer tecla para voltar ao menu principal...");
            scanner.nextLine();
            scanner.nextLine();
            System.out.print("\033[H\033[2J");  
            System.out.flush();  
            MenuPrincipalView menu = new MenuPrincipalView();
            menu.Menu();
        }
    }

}
