package br.edu.up.Controllers;

import br.edu.up.Data.GravarDados;
import br.edu.up.Data.LerDados;
import br.edu.up.Models.Departamento;
import java.util.List;

public class DepartamentoController {

    private GravarDados gravar = new GravarDados();
    private LerDados ler = new LerDados();

    public Departamento ObterPorId(int departamentoId) throws Exception {
        List<Departamento> departamentos = ler.buscarDepartamentos();

        try {
            for (Departamento departamento : departamentos) {

                if (departamento.getId() == departamentoId)
                    ;
                return departamento;
            }
            throw new Exception("Não foi possível encontrar um departamento com esse Id");
        } catch (Exception e) {
            throw new Exception("Erro ao localizar o departamento." + e.getMessage());
        }

    }

    public void ExibirListaDepartamentos() {
        List<Departamento> departamentos = ler.buscarDepartamentos();
        System.out.println("       DEPARTAMENTOS        ");
        System.out.println("----------------------------");
        System.out.println("|   Id   |    Descricao    |");
        System.out.println("----------------------------");
        for (Departamento departamento : departamentos) {
            System.out.println(departamento.getId() + "        | " + departamento.getDescricao());
        }
    }

    public List<Departamento> BuscarListaDepartamentos() {
        return ler.buscarDepartamentos();
    }

    public void GravarNovoDepartamento(Departamento departamento) throws Exception {
        try {
            gravar.GravarDepartamento(departamento);
            System.out.println("Nova função cadastrada: ID: " + departamento.getId() + "DESCRICAO: "
                    + departamento.getDescricao());
        } catch (Exception e) {
            throw new Exception("Erro ao cadastrar nova funcao" + e.getMessage());
        }
    }
}
