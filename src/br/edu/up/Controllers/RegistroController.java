package br.edu.up.Controllers;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;

import br.edu.up.Data.GravarDados;
import br.edu.up.Data.LerDados;
import br.edu.up.Models.Registro;

public class RegistroController {

    private GravarDados gravar = new GravarDados();
    private LerDados ler = new LerDados();

    public List<Registro> BuscarListaRegistros() throws Exception {
        return ler.buscarRegistros();
    }

    public void GravarNovoRegistro(Registro registro) throws Exception {
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss", new Locale("pt", "BR"));

            var pontoRegistrado = gravar.GravarNovoRegistro(registro);
            System.out.println("");
            System.out.println("");
            System.out.println("| ---------------------------------------- |");
            System.out.println("|       Registro gravado com sucesso.      |");
            System.out.println("| ---------------------------------------- |");
            System.out.println("| Id: " + pontoRegistrado.getId());
            System.out.println("| Funcionário: " + pontoRegistrado.getFuncionario().getNome());
            System.out.println("| Data: " + pontoRegistrado.Data.format(formatter));
            System.out.println("| Motivo: " + pontoRegistrado.getMotivo());
            System.out.println("|__________________________________________");
        } catch (Exception e) {
            throw new Exception("Erro ao cadastrar nova funcao" + e.getMessage());
        }
    }

    public void ExibirListaRegistros() throws Exception {
        List<Registro> registros = ler.buscarRegistros();
    
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss", new Locale("pt", "BR"));
    
        System.out.println("                                   REGISTROS                                         ");
        System.out.println("---------------------------------------------------------------------------------------------");
        System.out.printf("|   Id   |         Data        |              Nome              |            Motivo         |\n");
        System.out.println("---------------------------------------------------------------------------------------------");
    
        for (Registro registro : registros) {
            String idFormatado = String.format("%6d", registro.getId()); 
            String dataFormatada = registro.getData().format(formatter);
            String nomeFormatado = String.format("%-30s", registro.getFuncionario().getNome()); 
            String motivoFormatado = String.format("%-25s", registro.getMotivo()); 
    
            System.out.printf("| %s | %s | %s | %s |\n", idFormatado, dataFormatada, nomeFormatado, motivoFormatado);
        }
    
        System.out.println("----------------------------------------------------------------------------------------------");
    }


    public void ExibirListaRegistrosPorPeriodo(LocalDateTime inicio, LocalDateTime fim) throws Exception {
        List<Registro> registros = ler.buscarRegistrosPorPeroodo(inicio, fim);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss", new Locale("pt", "BR"));
        System.out.print("\033[H\033[2J");  
        System.out.flush();  
        System.out.println("                                   REGISTROS                                         ");
        System.out.println("---------------------------------------------------------------------------------------------");
        System.out.printf("|   Id   |         Data        |              Nome              |            Motivo         |\n");
        System.out.println("---------------------------------------------------------------------------------------------");
    
        for (Registro registro : registros) {
            String idFormatado = String.format("%6d", registro.getId()); 
            String dataFormatada = registro.getData().format(formatter);
            String nomeFormatado = String.format("%-30s", registro.getFuncionario().getNome()); 
            String motivoFormatado = String.format("%-25s", registro.getMotivo()); 
    
            System.out.printf("| %s | %s | %s | %s |\n", idFormatado, dataFormatada, nomeFormatado, motivoFormatado);
        }
    
        System.out.println("----------------------------------------------------------------------------------------------");
    }

}
