package br.edu.up.Controllers;

import br.edu.up.Data.GravarDados;
import br.edu.up.Data.LerDados;
import br.edu.up.Models.Funcionario;
import java.util.List;

public class FuncionarioController {

    private GravarDados gravar = new GravarDados();
    private LerDados ler = new LerDados();

    public void GravarNovoFuncionario(Funcionario funcionario) throws Exception {

        try {
            var fucionarioCadastrado = gravar.GravarFuncionario(funcionario);
            System.out.println("");
            System.out.println("");
            System.out.println("| ----------------------------------- |");
            System.out.println("| Funcionário cadastrado com sucesso. |");
            System.out.println("| ----------------------------------- |");
            System.out.println("| Id: " + fucionarioCadastrado.getId());
            System.out.println("| Matrícula: " + fucionarioCadastrado.getMatricula());
            System.out.println("| Nome: " + fucionarioCadastrado.getNome());
            System.out.println("| Departamento: " + fucionarioCadastrado.getDepartamento());
            System.out.println("| Função: " + fucionarioCadastrado.getFuncao() );
            System.out.println("|______________________________________");
        } catch (Exception ex) {
            throw new Exception("Erro ao cadastrar novo funcionário" + ex.getMessage());
        }

    }

    public Funcionario ObterPorMatricula(int matricula) throws Exception {
        List<Funcionario> funcionarios = ler.buscarFuncionarios();

        try {
            for (Funcionario funcionario : funcionarios) {

                if (funcionario.getMatricula() == matricula)
                {
                    return funcionario;
                }
            }
            throw new Exception("Não foi possível encontrar um funcionário com esse Id");
        } catch (Exception e) {
            throw new Exception("Erro ao localizar o departamento." + e.getMessage());
        }

    }

    public Funcionario ObterPorId(int id) throws Exception {
        List<Funcionario> funcionarios = ler.buscarFuncionarios();

        try {
            for (Funcionario funcionario : funcionarios) {

                if (funcionario.getId() == id)
                {
                    return funcionario;
                }
            }
            throw new Exception("Não foi possível encontrar um funcionário com esse Id");
        } catch (Exception e) {
            throw new Exception("Erro ao localizar o departamento." + e.getMessage());
        }

    }

    public void ExibirListaFuncionarios() throws Exception {
        List<Funcionario> funcionarios = ler.buscarFuncionarios();
        System.out.println("                                      FUNCIONÁRIOS                                         ");
        System.out.println(
                "----------------------------------------------------------------------------------------------");
        System.out.println(
                "|   Id   |    Matricula    |           Nome            |    Departamento   |       Funcao     |");
        System.out.println(
                "-----------------------------------------------------------------------------------------------");
        for (Funcionario funcionario : funcionarios) {
            System.out.println(funcionario.getId() + "        | " + funcionario.getMatricula() + "           | "
                    + funcionario.getNome() + "                    | " + funcionario.getDepartamento()
                    + "                | "
                    + funcionario.getFuncao());
        }
    }

    public List<Funcionario> BuscarListaFuncionarios() throws Exception {
        try {
            return ler.buscarFuncionarios();
        } catch (Exception e) {
            throw new Exception("Erro ao buscar lista de funcionário." + e.getMessage());
        }
    }

    public Object BuscarListaDepartamentos() {
        throw new UnsupportedOperationException("Unimplemented method 'BuscarListaDepartamentos'");
    }
}
