package br.edu.up.Controllers;

import br.edu.up.Data.GravarDados;
import br.edu.up.Data.LerDados;
import br.edu.up.Models.Funcao;
import java.util.List;

public class FuncaoController {

    private GravarDados gravar = new GravarDados();
    private LerDados ler = new LerDados();

    public Funcao ObterPorId(int funcaoId) throws Exception {
        List<Funcao> funcoes = ler.buscarFuncoes();

        try {
            for (Funcao funcao : funcoes) {

                if (funcao.getId() == funcaoId)
                    ;
                return funcao;
            }
            throw new Exception("Não foi possível encontrar um departamento com esse Id");
        } catch (Exception e) {
            throw new Exception("Erro ao localizar o departamento." + e.getMessage());
        }
    }

    public void ExibirListaFuncoes() {
        List<Funcao> funcoes = ler.buscarFuncoes();
        System.out.println("          FUNÇÕES           ");
        System.out.println("----------------------------");
        System.out.println("|   Id   |    Descricao    |");
        System.out.println("----------------------------");
        for (Funcao funcao : funcoes) {
            System.out.println(funcao.getId() + "        | " + funcao.getDescricao());
        }
    }

    public List<Funcao> BuscarListaFuncao() {
        return ler.buscarFuncoes();
    }

    public void GravarNovaFuncao(Funcao funcao) throws Exception {
        try {
            var funcaoCadastrada = gravar.GravarFuncao(funcao);
            System.out.println("");
            System.out.println("");
            System.out.println("| ----------------------------------- |");
            System.out.println("| Função cadastrada com sucesso. |");
            System.out.println("| ----------------------------------- |");
            System.out.println("| Id: " + funcaoCadastrada.getId());
            System.out.println("| Matrícula: " + funcaoCadastrada.getDescricao());
            System.out.println("|______________________________________");
        } catch (Exception e) {
            throw new Exception("Erro ao cadastrar nova funcao" + e.getMessage());
        }
    }

}
