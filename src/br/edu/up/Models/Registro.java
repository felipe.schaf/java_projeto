package br.edu.up.Models;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Registro {

    public int Id = 0;
    public Funcionario Funcionario = null;
    public LocalDateTime Data = null;
    public String Motivo = "";

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public Funcionario getFuncionario() {
        return Funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        Funcionario = funcionario;
    }

    public LocalDateTime getData() {
        return Data;
    }

    public void setData(LocalDateTime data) {
        Data = data;
    }

    public String getMotivo() {
        return Motivo;
    }

    public void setMotivo(String motivo) {
        Motivo = motivo;
    }

    public String toCSV() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
        String dataFormatada = Data.format(formatter);
        return Id + ";" + Funcionario.getId() + ";" + dataFormatada + ";" + Motivo + ";";
    }

}
