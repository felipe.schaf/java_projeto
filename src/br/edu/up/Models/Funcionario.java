package br.edu.up.Models;

public class Funcionario {
    private int Id = 0;
    private int Matricula = 0;
    private String Nome = "";
    private Departamento Departamento = null;
    private Funcao Funcao = null;

    public Funcionario() {
    }

    public Funcionario(int id, int matricula, String nome, Departamento departamento, Funcao funcao) {
        this.Id = id;
        this.Matricula = matricula;
        this.Nome = nome;
        this.Departamento = departamento;
        this.Funcao = funcao;
    }

    public Funcionario(int matricula, String nome, Departamento departamento, Funcao funcao) {
        this.Matricula = matricula;
        this.Nome = nome;
        this.Departamento = departamento;
        this.Funcao = funcao;
    }

    public int getId() {
        return Id;
    }

    public int getMatricula() {
        return Matricula;
    }

    public String getNome() {
        return Nome;
    }

    public String getDepartamento() {
        return Departamento.getDescricao();
    }

    public String getFuncao() {
        return Funcao.getDescricao();
    }

    public void setId(int id) {
        Id = id;
    }

    public void setMatricula(int matricula) {
        Matricula = matricula;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    public void setDepartamento(Departamento departamento) {
        Departamento = departamento;
    }

    public void setFuncao(Funcao funcao) {
        Funcao = funcao;
    }

    public String toCSV() {
        return Id + ";" + Matricula + ";" + Nome + ";" + Departamento.Id + ";" + Funcao.Id + ";";
    }
}
