package br.edu.up.Models;

public class Departamento {
    public int Id;
    public String Descricao = "";

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getDescricao() {
        return Descricao;
    }

    public void setDescricao(String Descricao) {
        this.Descricao = Descricao;
    }

    public String toCSV() {
        return Id + ";" + Descricao + ";";
    }
}
