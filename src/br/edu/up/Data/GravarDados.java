package br.edu.up.Data;

import br.edu.up.Controllers.DepartamentoController;
import br.edu.up.Controllers.FuncaoController;
import br.edu.up.Controllers.FuncionarioController;
import br.edu.up.Controllers.RegistroController;
import br.edu.up.Models.Departamento;
import br.edu.up.Models.Funcao;
import br.edu.up.Models.Funcionario;
import br.edu.up.Models.Registro;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class GravarDados {

    public String ArquivosCaminho = "D:\\Estudos\\java_projeto\\src\\br\\edu\\up\\Arquivos\\";
    // public String ArquivosCaminho =
    // "C:\\java_projeto\\ProjetoSistemaPonto\\src\\br\\edu\\up\\Arquivos\\";

    public Scanner scanner = new Scanner(System.in);

    public Funcionario GravarFuncionario(Funcionario funcionario) throws Exception {
        FuncionarioController funcionarioController = new FuncionarioController();

        var arquivo = ArquivosCaminho + "Funcionarios.csv\\";

        try {
            FileWriter arquivoGravar = new FileWriter(arquivo, true);
            PrintWriter gravador = new PrintWriter(arquivoGravar);

            var lista = funcionarioController.BuscarListaFuncionarios();
            if (!lista.isEmpty()) {
                var ultimoRegistro = lista.get(lista.size() - 1);
                int ultimoId = ultimoRegistro.getId();
                funcionario.setId(ultimoId = ultimoId + 1);
            } else {
                funcionario.setId(1);
            }

            gravador.println(funcionario.toCSV());
            gravador.close();

            return funcionario;

        } catch (IOException e) {
            System.out.println("Não foi possível gravar o arquivo!" + e.getMessage());
        }

        return null;
    }

    public boolean GravarDepartamento(Departamento departamento) {

        DepartamentoController departamentoController = new DepartamentoController();
        var arquivo = ArquivosCaminho + "Departamentos.csv\\";

        try {
            FileWriter arquivoGravar = new FileWriter(arquivo, true);
            PrintWriter gravador = new PrintWriter(arquivoGravar);

            var lista = departamentoController.BuscarListaDepartamentos();

            if (!lista.isEmpty()) {
                var ultimoRegistro = lista.get(lista.size() - 1);
                int ultimoId = ultimoRegistro.getId();
                departamento.setId(ultimoId = ultimoId + 1);
            } else {
                departamento.setId(1);
            }

            gravador.println(departamento.toCSV());
            gravador.close();

            return true;

        } catch (Exception e) {
            System.out.println("Não foi possível gravar o arquivo!");
        }

        return false;
    }

    public Funcao GravarFuncao(Funcao funcao) {

        FuncaoController funcaoController = new FuncaoController();
        var arquivo = ArquivosCaminho + "Funcoes.csv\\";

        try {
            FileWriter arquivoGravar = new FileWriter(arquivo, true);
            PrintWriter gravador = new PrintWriter(arquivoGravar);

            var lista = funcaoController.BuscarListaFuncao();

            if (!lista.isEmpty()) {
                var ultimoRegistro = lista.get(lista.size() - 1);
                int ultimoId = ultimoRegistro.getId();
                funcao.setId(ultimoId = ultimoId + 1);
            } else {
                funcao.setId(1);
            }

            gravador.println(funcao.toCSV());
            gravador.close();

            return funcao;

        } catch (Exception e) {
            System.out.println("Não foi possível gravar o arquivo!");
        }

        return null;
    }

    public Registro GravarNovoRegistro(Registro registro) throws Exception {
        try {
            File arquivo = new File(ArquivosCaminho + "Registros.csv");
            FileWriter arquivoGravar = new FileWriter(arquivo, true);
            PrintWriter gravador = new PrintWriter(arquivoGravar);

            RegistroController registroController = new RegistroController();
            var lista = registroController.BuscarListaRegistros();

            if (!lista.isEmpty()) {
                var ultimoRegistro = lista.get(lista.size() - 1);
                int ultimoId = ultimoRegistro.getId();
                registro.setId(ultimoId = ultimoId + 1);
            } else {
                registro.setId(1);
            }

            gravador.println(registro.toCSV());
            gravador.close();

            return registro;

        } catch (IOException e) {
            System.out.println("Não foi possível gravar o arquivo! " + e.getMessage());
        }

        return null;
    }

}
