package br.edu.up.Data;

import br.edu.up.Controllers.DepartamentoController;
import br.edu.up.Controllers.FuncaoController;
import br.edu.up.Controllers.FuncionarioController;
import br.edu.up.Models.Departamento;
import br.edu.up.Models.Funcao;
import br.edu.up.Models.Funcionario;
import br.edu.up.Models.Registro;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class LerDados {

    public String ArquivosCaminho = "D:\\Estudos\\java_projeto\\src\\br\\edu\\up\\Arquivos\\";

    public Scanner scanner = new Scanner(System.in);

    public List<Funcionario> buscarFuncionarios() throws Exception {

        List<Funcionario> listaDeFuncionarios = new ArrayList<>();

        try {
            File arquivoLeitura = new File(ArquivosCaminho + "Funcionarios.csv");
            Scanner leitor = new Scanner(arquivoLeitura);

            while (leitor.hasNextLine()) {
                String linha = leitor.nextLine();
                String[] dados = linha.split(";");

                int id = Integer.parseInt(dados[0]);
                int matricula = Integer.parseInt(dados[1]);
                String nome = dados[2];

                int departamentoId = Integer.parseInt(dados[3]);
                int funcaoId = Integer.parseInt(dados[4]);

                DepartamentoController departamentoController = new DepartamentoController();
                var departamento = departamentoController.ObterPorId(departamentoId);

                FuncaoController funcaoController = new FuncaoController();
                var funcao = funcaoController.ObterPorId(funcaoId);

                Funcionario funcionario = new Funcionario(id, matricula, nome, departamento, funcao);
                listaDeFuncionarios.add(funcionario);
            }

            leitor.close();
        } catch (FileNotFoundException e) {

            System.out.println("Arquivo não encontrado! " + e.getMessage());
        }

        return listaDeFuncionarios;
    }

    public List<Departamento> buscarDepartamentos() {

        List<Departamento> listaDepartamentos = new ArrayList<>();

        try {
            File arquivoLeitura = new File(ArquivosCaminho + "Departamentos.csv");
            Scanner leitor = new Scanner(arquivoLeitura);

            while (leitor.hasNextLine()) {
                String linha = leitor.nextLine();
                String[] dados = linha.split(";");

                int Id = Integer.parseInt(dados[0]);
                String Descricao = dados[1];

                Departamento departamento = new Departamento();
                departamento.setId(Id);
                departamento.setDescricao(Descricao);

                listaDepartamentos.add(departamento);
            }

            leitor.close();
        } catch (FileNotFoundException e) {
            System.out.println("Arquivo não encontrado! " + e.getMessage());
        }

        return listaDepartamentos;
    }

    public List<Funcao> buscarFuncoes() {

        List<Funcao> listaFuncoes = new ArrayList<>();

        try {
            File arquivoLeitura = new File(ArquivosCaminho + "Funcoes.csv");
            Scanner leitor = new Scanner(arquivoLeitura);

            while (leitor.hasNextLine()) {
                String linha = leitor.nextLine();
                String[] dados = linha.split(";");

                int Id = Integer.parseInt(dados[0]);
                String Descricao = dados[1];

                Funcao funcao = new Funcao();
                funcao.setId(Id);
                funcao.setDescricao(Descricao);

                listaFuncoes.add(funcao);
            }

            leitor.close();
        } catch (FileNotFoundException e) {
            System.out.println("Arquivo não encontrado! " + e.getMessage());
        }

        return listaFuncoes;
    }

    public List<Registro> buscarRegistros() throws Exception {
        List<Registro> listaDeRegistros = new ArrayList<>();

        try {
            File arquivoLeitura = new File(ArquivosCaminho + "Registros.csv");
            Scanner leitor = new Scanner(arquivoLeitura);

            while (leitor.hasNextLine()) {
                String linha = leitor.nextLine();
                String[] dados = linha.split(";");

                int id = Integer.parseInt(dados[0]);
                int funcionarioId = Integer.parseInt(dados[1]);
                String data = dados[2];
                String motivo = dados[3];

                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
                LocalDateTime dateTime = LocalDateTime.parse(data, formatter);

                FuncionarioController funcionarioController = new FuncionarioController();
                var funcionario = funcionarioController.ObterPorId(funcionarioId);

                Registro registro = new Registro();
                registro.Id = id;
                registro.Funcionario = funcionario;
                registro.Data = dateTime;
                registro.Motivo = motivo;

                listaDeRegistros.add(registro);
            }

            leitor.close();
        } catch (FileNotFoundException e) {
            System.out.println("Arquivo não encontrado! " + e.getMessage());
        }

        return listaDeRegistros;
    }

    public List<Registro> buscarRegistrosPorPeroodo(LocalDateTime inicio, LocalDateTime fim) throws Exception {
        List<Registro> listaDeRegistros = new ArrayList<>();

        try {
            File arquivoLeitura = new File(ArquivosCaminho + "Registros.csv");
            Scanner leitor = new Scanner(arquivoLeitura);

            while (leitor.hasNextLine()) {
                String linha = leitor.nextLine();
                String[] dados = linha.split(";");

                int id = Integer.parseInt(dados[0]);
                int funcionarioId = Integer.parseInt(dados[1]);
                String data = dados[2];
                String motivo = dados[3];

                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
                LocalDateTime dateTime = LocalDateTime.parse(data, formatter);

                FuncionarioController funcionarioController = new FuncionarioController();
                var funcionario = funcionarioController.ObterPorId(funcionarioId);

                Registro registro = new Registro();
                registro.Id = id;
                registro.Funcionario = funcionario;
                registro.Data = dateTime;
                registro.Motivo = motivo;
                if (registro.getData().isAfter(inicio) && registro.getData().isBefore(fim)) {
                    listaDeRegistros.add(registro);
                }
            }

            leitor.close();
        } catch (FileNotFoundException e) {
            System.out.println("Arquivo não encontrado! " + e.getMessage());
        }

        return listaDeRegistros;
    }

}
